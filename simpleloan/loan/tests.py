from django.contrib.auth.models import User
from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status

from .models import LoanRequest


class LoanRequestTestCase(TestCase):
    def setUp(self):
        self.client = APIClient()
        self.superuser = User(
            username='Yennefer',
            email='yennefer@ofvengerberg.com',
            is_staff=True
        )
        self.superuser.set_password('supersafekeyword')
        self.superuser.save()
        self.loan_request = LoanRequest.objects.create(
            first_name='Geralt',
            last_name='Of Rivia',
            gender='M',
            email='geralt@ofrivia.com',
            requested_amount=15000,
            id_number=30156149,
            pre_score='R'
        )

    def test_annonymous_user_can_create_loan_request(self):
        """Authentification is not required to create a loan request."""
        response = self.client.post(
            '/loan/loan-request/',
            {
                'first_name': 'Cirilla',
                'last_name': 'Fiona Elen Riannon',
                'gender': 'F',
                'email': 'cirilla@riannon.com',
                'requested_amount': 51000,
                'id_number': 30156149
            },
            format='json'
        )
        lr_id = response.json().get('id', None)
        self.assertEqual(isinstance(lr_id, int), True)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(LoanRequest.objects.filter(id=lr_id).exists(), True)

    def test_user_staff_can_delete_loan_request(self):
        self.client.force_authenticate(user=self.superuser)
        lr_id = self.loan_request.id
        url = f'/loan/loan-request-admin/{lr_id}/'
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(LoanRequest.objects.filter(id=lr_id).exists(), False)


    def test_user_staff_can_edit_loan_request(self):
        self.client.force_authenticate(user=self.superuser)
        lr_id = self.loan_request.id
        url = f'/loan/loan-request-admin/{lr_id}/'
        response = self.client.patch(
            url,
            {
                'first_name': 'Geralt',
                'last_name': 'Of Rivia',
                'gender': 'M',
                'email': 'geralt@ofrivia.com',
                'requested_amount':0,
                'id_number': 30156150,
                'pre_score': 'R'
            },
            format='json'
        )
        self.loan_request.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(self.loan_request.requested_amount, 0)
