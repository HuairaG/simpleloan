from django.db import models
from django.utils.translation import gettext_lazy as _


class LoanRequest(models.Model):
    """Stores a single loan request."""
    GENDER_CHOICES = (
        ('M', _('Male')),
        ('F', _('Female')),
    )
    SCORE_CHOICES = (
        ('A', _('Accepted')),
        ('R', _('Rejected')),
    )
    first_name = models.CharField(_("First Name"), max_length=50)
    last_name = models.CharField(_("Last Name"), max_length=50)
    email = models.EmailField(_("Email"), max_length=254)
    id_number = models.PositiveIntegerField(_("ID Number"))
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    requested_amount = models.DecimalField(
        _("Requested Amount"),
        max_digits=10,
        decimal_places=2
    )
    pre_score = models.CharField(max_length=1, choices=SCORE_CHOICES)

    class Meta:
        verbose_name = _("Loan Request")
        verbose_name_plural = _("Loan Requests")
