from django.shortcuts import render
from rest_framework import viewsets
from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.response import Response
from rest_framework.generics import CreateAPIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from .models import LoanRequest
from .serializers import LoanRequestSerializer

class LoanRequestViewSet(viewsets.ModelViewSet):
    """ViewSet for admin users to manage loan requests."""
    http_method_names = ['get', 'patch', 'delete','options', 'head']
    serializer_class = LoanRequestSerializer
    queryset = LoanRequest.objects.all()
    permission_classes = (IsAdminUser,)
    authentication_class = (JSONWebTokenAuthentication,)

class LoanRequestCreateApiView(CreateAPIView):
    """View to process and create all loan requests."""
    permissions_classes = (AllowAny,)
    serializer_class = LoanRequestSerializer
    authentication_classes = []

