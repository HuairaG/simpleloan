from django.urls import path
from rest_framework import routers

from .views import LoanRequestCreateApiView, LoanRequestViewSet

app_name = 'loan'

router = routers.DefaultRouter()
router.register(
    'loan-request-admin',
    LoanRequestViewSet,
    basename='loan_request'
)

urlpatterns = [
    path(
        'loan-request/',
        LoanRequestCreateApiView.as_view(),
        name='loan_request'
    ),
    *router.urls
]