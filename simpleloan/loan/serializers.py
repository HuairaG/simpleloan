from rest_framework import serializers

from .models import LoanRequest
from .utils import get_pre_score

class LoanRequestSerializer(serializers.ModelSerializer):

    pre_score = serializers.ChoiceField(
        required=False,
        choices=LoanRequest.SCORE_CHOICES
    )

    class Meta:
        model = LoanRequest
        fields = [
            'id',
            'first_name',
            'last_name',
            'email',
            'gender',
            'requested_amount',
            'id_number',
            'pre_score'
        ]

    def validate(self, data):
        data = super().validate(data)
        id_number = data.get('id_number')
        pre_score = data.get('pre_score', None)
        if not self.instance:
            pre_score = get_pre_score(id_number)
        if pre_score is None:
            raise serializers.ValidationError(
                'Could\'t get the pre-score. Try again later.'
            )
        data['pre_score'] = pre_score
        return data
