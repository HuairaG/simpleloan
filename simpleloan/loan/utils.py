import requests
from django.conf import settings
from rest_framework import status

def get_pre_score(id_number):
    """Get a person pre-score."""
    url = f'{settings.PRE_SCORE_URL}{id_number}'
    headers = {'credential': settings.PRE_SCORE_CREDENTIAL}
    try:
        res = requests.get(url, timeout=20, headers=headers)
    except requests.exceptions.Timeout:
        return None
    if res.status_code == status.HTTP_200_OK and not res.json().get('has_error'):
        pre_status = res.json().get("status")
        return 'A' if pre_status == 'approve' else 'R'
